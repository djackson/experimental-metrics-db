select min(rowid),fingerprint,start,end,write_history_values from extra_info_relay_write_history GROUP BY fingerprint,start,end

delete   from extra_info_relay_write_history
where    rowid not in
         (
         select  min(rowid)
         from    extra_info_relay_write_history
		 GROUP BY fingerprint,start,end,write_history_values
         )

select fingerprint,start,end,min(write_history_values)-max(write_history_values) from extra_info_relay_write_history GROUP BY fingerprint,start,end 

#TODO 
Source of duplicate discrepancy?
Are there overlapping records missed by this check? e.g. two fingerprints on the same day? 
Write a quick script for what level of duplication is best? 
Add indexes for start/end.
