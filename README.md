# Tor Metrics DB (Experimental)

**Goal:** Produce a single database containing all information stored by CollecTor (e.g. server descriptors, extrainfo, past consensuses, bandwidth measurements).

**Method:** Ingest records from CollecTor, parse them using Stem and write them out to a database (SQLite or PostgreSQL).

**Status:** Functional. Accuracy has not been rigorously tested!

Currently, the tool handles consensus documents, server descriptors and all types of extra-info statistics.
Partial support is implemented for bandwidth files.

TODOs
 [ ] - Finish implementing bandwidth files.
 [ ] - Add support for onionperf files?
 [ ] - Provide a view of each table normalised by day and by relay.
 [ ] - Provide a merged view where all important values for a particular relay can be seen at day granularity. 

## Usage

Minimal Example:

```
./tor-metrics-db.py --start DD-MM-YYYY 
```
The script will fetch all supported files from CollecTor and cache them in `data/` then process them into a sqlite database located in `out/`. A week's worth of records takes around one hour to process, but then querying the data is much much faster.

See `tor-metrics-db.py --help` for more information: 

```
usage: tor-metrics-db.py [-h] [-db DATABASE] --start START [--end END] [--only {network-status-consensus-3,server-descriptor,extra-info,bandwidth-file,ALL}] [--cache CACHE]
                         [--cache-only] [--fresh] [--index]

A script to fetch records collected by CollecTor, parse them using Stem and store them in a database

optional arguments:
  -h, --help            show this help message and exit
  -db DATABASE, --database DATABASE
                        The sqlite or postgres database url you would like to output to
  --start START         The date DD-MM-YYYY you would like to begin from
  --end END             The date DD-MM-YYYY you would like to end from
  --only {network-sstatus-consensus-3,server-descriptor,extra-info,bandwidth-file,ALL}
                        The types of documents you would like to parse.
  --cache CACHE         A directory in which to store temporary files downloaded from CollectTor, speeding up subsequent runs.
  --cache-only          Don't attempt to download files from the network.
  --fresh               Drop the database and the list of parsed CollecTor files.
  --index               Build indexes for each table.
```

## Stack

* Python3
* [Dataset](https://dataset.readthedocs.io/en/latest/) - Simple abstraction layer for working with databases.  
* [CollecTor](https://metrics.torproject.org/collector.html) - Tor's Metrics service.  
* [Stem](https://stem.torproject.org/) - Python library for interacting with Tor.
