from datetime import datetime, timedelta
import stem.descriptor.collector
from tqdm import tqdm


def get_entries(name, start_date, end_date, cache_dir):
    if name in ["network-status-consensus-3"]:
        document_handler = "DOCUMENT"
    else:
        document_handler = None
    collector = stem.descriptor.collector.CollecTor()
    for f in collector.files(name, start=start_date, end=end_date):
        f.download(cache_dir, overwrite=True)
    for f in collector.files(name, start=start_date, end=end_date):
        for desc in f.read(cache_dir, document_handler=document_handler):
            yield desc


if __name__ == "__main__":
    startT = datetime.now()
    for x in tqdm(
        get_entries(
            "server-descriptor", datetime.now() - timedelta(days=7), None, "data/"
        ),
        leave=True,
    ):
        pass
    endT = datetime.now()
    print(f"Delta: {endT - startT}")