import sqlite3
import datetime
import dateutil.parser


def get_relays(cursor, table):
    cursor.execute("SELECT DISTINCT fingerprint FROM " + table)
    for x in cursor.fetchall():
        yield x[0]


def discoverColNames(cursor, table):
    cursor.execute("SELECT * FROM " + table)
    names = [description[0] for description in cursor.description]
    endCandidates, startCandidates = list(), list()
    for n in names:
        if n.endswith("end"):
            endCandidates.append(n)
        if n.endswith("start"):
            startCandidates.append(n)
    if (
        len(endCandidates) > 1
        or len(startCandidates) > 1
        or len(endCandidates) + len(startCandidates) < 2
    ):
        return None, None
    return startCandidates[0], endCandidates[0]


def get_first_last_date(cursor, fingerprint, table, startCol=None, endCol=None):
    if startCol is None or endCol is None:
        startCol, endCol = discoverColNames(cursor, table)
    f = "SELECT :startCol FROM :table WHERE fingerprint = ':fingerprint' ORDER BY :startCol ASC LIMIT 1;"
    l = "SELECT :endCol FROM :table WHERE fingerprint = ':fingerprint' ORDER BY :endCol DESC LIMIT 1;"
    f = (
        f.replace(":startCol", startCol)
        .replace(":table", table)
        .replace(":fingerprint", fingerprint)
    )
    l = (
        l.replace(":table", table)
        .replace(":endCol", endCol)
        .replace(":fingerprint", fingerprint)
    )
    cursor.execute(f)
    sc = cursor.fetchall()[0][0]
    cursor.execute(l)
    ec = cursor.fetchall()[0][0]
    return sc, ec


def get_records(cursor, fingerprint, table, date, startCol=None, endCol=None):
    q = """
    SELECT DISTINCT * FROM :table WHERE 
    fingerprint = :fingerprint
    AND 
    ((:startCol < :endOfDay AND :startCol >= :startOfDay)
    OR
    (:endCol > :startOfDay AND :endCol <= :endOfDay))
    """
    q = q.replace(":table", table)
    if startCol is None or endCol is None:
        startCol, endCol = discoverColNames(cursor, table)
    q = q.replace(
        ":startCol", startCol
    )  # TODO - What's the right way to put in column names?
    q = q.replace(":endCol", endCol)
    qDict = {"fingerprint": fingerprint, "startCol": startCol, "endCol": endCol}
    qDict["startOfDay"] = date
    qDict["endOfDay"] = qDict["startOfDay"] + datetime.timedelta(days=1)
    print(f"Checking {qDict['startOfDay']} and {qDict['endOfDay']}")
    cursor.execute(q, qDict)
    return cursor.fetchall()


if __name__ == "__main__":
    dbOld = sqlite3.connect("../out/data.db")
    dbNew = sqlite3.connect("../out/data_norm.db")
    dbOld.row_factory = sqlite3.Row
    dbNew.row_factory = sqlite3.Row

    cOld = dbOld.cursor()
    cNew = dbNew.cursor()

    print("WARNING: This script does not attempt to mitigate SQL injection")

    tables = ["relay_write_history"]
    for t in tables:
        startCol, endCol = discoverColNames(cOld, t)
        print(f"Considering {t} with columns {startCol} and {endCol}")
        for f in get_relays(cOld, t):
            fD, lD = get_first_last_date(cOld, f, t, startCol=startCol, endCol=endCol)
            print(f"{f} has records between {fD} and {lD}")
            cD = dateutil.parser.parse(fD).date()
            lD = dateutil.parser.parse(lD).date()
            while cD < lD:
                r = get_records(cOld, f, t, cD)
                cD = cD + datetime.timedelta(days=1)
                print(f"{cD} has {len(r)} records")
                if len(r) >= 5:
                    import code

                    code.interact(local=locals())
