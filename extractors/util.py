import datetime


def extract_series(desc, endpointS, intervalS, values, core, db_prefix=""):
    entries = list()
    endpoint = getattr(desc, endpointS)
    if endpoint is None:
        return list()
    diffSeconds = getattr(desc, intervalS)
    vDict = dict()  # TODO Remove this is ugly
    for v in values:
        lVal = getattr(desc, v)
        lVal.reverse()  # As the values are stored oldest to newest
        vDict[v] = lVal
    for i in range(len(vDict[values[0]])):
        entry = dict(core)
        entry["start"] = endpoint - datetime.timedelta(seconds=((i + 1) * diffSeconds))
        entry["end"] = endpoint - datetime.timedelta(seconds=((i) * diffSeconds))
        for v in values:
            entry[db_prefix + v] = vDict[v][i]
        entries.append(entry)
    return entries


def extract_dict(desc, dict_name, key_name, val_name, core):
    entries = list()
    for k, v in getattr(desc, dict_name).items():
        r = dict(core)
        r[key_name] = k
        r[val_name] = v
        entries.append(r)
    return entries


def has_attribute(desc, attrib, whitelist=None):
    if (
        whitelist is not None
        and len(whitelist) > 0
        and attrib not in whitelist
        and attrib.rstrip("_end") not in whitelist
    ):
        return False
    else:
        return hasattr(desc, attrib) and getattr(desc, attrib) is not None
