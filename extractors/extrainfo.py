"""
Source: https://stem.torproject.org/api/descriptor/extrainfo_descriptor.html

Counter({'extra-info': 520553, 'identity-ed25519': 520553, 'published': 520553, 'router-sig-ed25519': 520553, 'router-signature': 520553, 'write-history': 512876, 'read-history': 512876, 'dirreq-read-history': 491375, 'dirreq-write-history': 491177, 'hidserv-stats-end': 442154, 'hidserv-rend-relayed-cells': 442154, 'hidserv-dir-onions-seen': 442154, 'padding-counts': 441583, 'geoip-db-digest': 392295, 'geoip6-db-digest': 391601, 'dirreq-stats-end': 320439, 'dirreq-v3-ips': 320439, 'dirreq-v3-reqs': 320438, 'dirreq-v3-resp': 320438, 'dirreq-v3-direct-dl': 320438, 'dirreq-v3-tunneled-dl': 320438, 'entry-stats-end': 9965, 'entry-ips': 9965, 'exit-stats-end': 7532, 'exit-kibibytes-written': 7532, 'exit-kibibytes-read': 7532, 'exit-streams-opened': 7532, 'cell-stats-end': 6830, 'cell-processed-cells': 6830, 'cell-queued-cells': 6830, 'cell-time-in-queue': 6830, 'cell-circuits-per-decile': 6830, 'conn-bi-direct': 6476, 'ipv6-read-history': 5549, 'ipv6-write-history': 5548, 'hidserv-v3-stats-end': 2443, 'hidserv-rend-v3-relayed-cells': 2443, 'hidserv-dir-v3-onions-seen': 2443, 'transport': 2436, 'ipv6-conn-bi-direct': 373})
"""
# TODO some of these appear to be missing? ipv6-?

import datetime
from .util import has_attribute, extract_series, extract_dict

"""
    fingerprint (str) -- * identity key fingerprint
    published (datetime) -- * time in UTC when this descriptor was made
    geoip_db_digest (str) -- sha1 of the geoIP database file for IPv4 addresses
    geoip6_db_digest (str) -- sha1 of the geoIP database file for IPv6 addresses
"""


def extract_core(desc):
    core_attributes = [
        "fingerprint",
        "published",
    ]  # ,'geoip_db_digest','geoip6_db_digest'] //TODO Are these values useful?
    out = dict()
    for k in core_attributes:
        out[k] = getattr(desc, k)
    return out


"""
Bi-directional connection usage:
    conn_bi_direct_end (datetime) -- end of the sampling interval
    conn_bi_direct_interval (int) -- seconds per interval
    conn_bi_direct_below (int) -- connections that read/wrote less than 20 KiB
    conn_bi_direct_read (int) -- connections that read at least 10x more than wrote
    conn_bi_direct_write (int) -- connections that wrote at least 10x more than read
    conn_bi_direct_both (int) -- remaining connections
"""


def extract_conn_bi_direct(desc, entry):
    conn_bi_direct = [
        "conn_bi_direct_below",
        "conn_bi_direct_read",
        "conn_bi_direct_write",
        "conn_bi_direct_both",
    ]
    for k in conn_bi_direct:
        entry[k] = getattr(desc, k)
    entry["end"] = desc.conn_bi_direct_end
    entry["start"] = desc.conn_bi_direct_end - datetime.timedelta(
        seconds=desc.conn_bi_direct_interval
    )
    return entry


"""
Bytes read/written for relayed traffic:
    read_history_end (datetime) -- end of the sampling interval
    read_history_interval (int) -- seconds per interval
    read_history_values (list) -- bytes read during each interval
    write_history_end (datetime) -- end of the sampling interval
    write_history_interval (int) -- seconds per interval
    write_history_values (list) -- bytes written during each interval
"""


def extract_relayed_traffic(desc, entry):
    readList = extract_series(
        desc,
        "read_history_end",
        "read_history_interval",
        ["read_history_values"],
        entry,
    )
    writeList = extract_series(
        desc,
        "write_history_end",
        "write_history_interval",
        ["write_history_values"],
        entry,
    )
    return (readList, writeList)


"""
Cell relaying statistics:
    cell_stats_end (datetime) -- end of the period when stats were gathered
    cell_stats_interval (int) -- length in seconds of the interval
    cell_processed_cells (list) -- measurement of processed cells per circuit
    cell_queued_cells (list) -- measurement of queued cells per circuit
    cell_time_in_queue (list) -- mean enqueued time in milliseconds for cells
    cell_circuits_per_decile (int) -- mean number of circuits in a decile
"""


def extract_relayed_cells(desc, entry):
    entry["cell_circuits_per_decile"] = getattr(desc, "cell_circuits_per_decile")
    return extract_series(
        desc,
        "cell_stats_end",
        "cell_stats_interval",
        ["cell_processed_cells", "cell_queued_cells", "cell_time_in_queue"],
        entry,
    )


"""
Bytes read/written for directory mirroring:
Variables:	

    dir_read_history_end (datetime) -- end of the sampling interval
    dir_read_history_interval (int) -- seconds per interval
    dir_read_history_values (list) -- bytes read during each interval
    dir_write_history_end (datetime) -- end of the sampling interval
    dir_write_history_interval (int) -- seconds per interval
    dir_write_history_values (list) -- bytes read during each interval
"""


def extract_dir_bytes(desc, entry):
    readList = extract_series(
        desc,
        "dir_read_history_end",
        "dir_read_history_interval",
        ["dir_read_history_values"],
        entry,
    )
    writeList = extract_series(
        desc,
        "dir_write_history_end",
        "dir_write_history_interval",
        ["dir_write_history_values"],
        entry,
    )
    return readList, writeList


"""
Guard Attributes:
Variables:	

    entry_stats_end (datetime) -- end of the period when stats were gathered
    entry_stats_interval (int) -- length in seconds of the interval
    entry_ips (dict) -- mapping of locales to rounded count of unique user ips
"""


def extract_guard_stats(desc, entry):
    entry["start"] = desc.entry_stats_end - datetime.timedelta(
        seconds=desc.entry_stats_interval
    )
    entry["end"] = desc.entry_stats_end
    return extract_dict(desc, "entry_ips", "locale", "rounded_unique_user_ips", entry)


"""
Exit Attributes:
Variables:	

    exit_stats_end (datetime) -- end of the period when stats were gathered
    exit_stats_interval (int) -- length in seconds of the interval
    exit_kibibytes_written (dict) -- traffic per port (keys are ints or 'other')
    exit_kibibytes_read (dict) -- traffic per port (keys are ints or 'other')
    exit_streams_opened (dict) -- streams per port (keys are ints or 'other')
"""


def extract_exit_stats(desc, entry):
    entry["end"] = desc.exit_stats_end
    entry["start"] = desc.exit_stats_end - datetime.timedelta(
        seconds=desc.exit_stats_interval
    )
    writtenList = extract_dict(
        desc, "exit_kibibytes_written", "port", "kibibytes", entry
    )
    readList = extract_dict(desc, "exit_kibibytes_read", "port", "kibibytes", entry)
    streamsList = extract_dict(
        desc, "exit_streams_opened", "port", "stream_count", entry
    )
    return writtenList, readList, streamsList


"""
Hidden Service Attributes:
Variables:	

    hs_stats_end (datetime) -- end of the sampling interval
    hs_rend_cells (int) -- rounded count of the RENDEZVOUS1 cells seen
    hs_rend_cells_attr (dict) -- * attributes provided for the hs_rend_cells
    hs_dir_onions_seen (int) -- rounded count of the identities seen
    hs_dir_onions_seen_attr (dict) -- * attributes provided for the hs_dir_onions_seen
"""


def extract_hs_v2_stats(desc, entry):
    # TODO No Start?
    keys = [
        "hs_stats_end",
        "hs_rend_cells",
        "hs_rend_cells_attr",
        "hs_dir_onions_seen",
        "hs_dir_onions_seen_attr",
    ]
    # TODO dict handling
    # {"delta_f": "2048", "epsilon": "0.30", "bin_size": "1024"}
    entry["version"] = 2
    for k in keys:
        entry[k] = getattr(desc, k)
    return entry


def extract_hs_v3_stats(desc, entry):
    hacky_extract = lambda x: desc._entries[x][0][0].split(" ")[0]
    entry["version"] = 3
    entry["hs_stats_end"] = datetime.datetime.strptime(
        hacky_extract("hidserv-v3-stats-end"), "%Y-%m-%d"
    )
    entry["hs_rend_cells"] = int(hacky_extract("hidserv-rend-v3-relayed-cells"))
    entry["hs_dir_onions_seen"] = int(hacky_extract("hidserv-dir-v3-onions-seen"))
    return entry


"""
Padding Count Attributes:
Variables:	

    padding_counts (dict) -- * padding parameters
    padding_counts_end (datetime) -- end of the period when padding data is being collected
    padding_counts_interval (int) -- length in seconds of the interval
"""


def extract_padding_counts(desc, entry):
    entry["end"] = desc.padding_counts_end
    entry["start"] = desc.padding_counts_end - datetime.timedelta(
        seconds=desc.padding_counts_interval
    )
    entryList = extract_dict(desc, "padding_counts", "parameter", "value", entry)
    return entryList


"""
Bridge Attributes:
Variables:	

    bridge_stats_end (datetime) -- end of the period when stats were gathered
    bridge_stats_interval (int) -- length in seconds of the interval
    bridge_ips (dict) -- mapping of locales to rounded count of unique user ips
    ip_versions (dict) -- mapping of ip protocols to a rounded count for the number of users
"""


def extract_bridge_data(desc, entry):
    entry["end"] = desc.bridge_stats_end
    entry["start"] = desc.bridge_stats_end - datetime.timedelta(
        seconds=desc.bridge_stats_interval
    )
    ips = extract_dict(desc, "bridge_ips", "locale", "rounded_user_ips", entry)
    versions = extract_dict(desc, "ip_versions", "protocols", "user_count", entry)
    return ips, versions


"""
Directory Mirror Attributes:
Variables:	
    dir_stats_end (datetime) -- end of the period when stats were gathered
    dir_stats_interval (int) -- length in seconds of the interval
    dir_v3_share (float) -- percent of total directory traffic it expects to serve
    dir_v3_responses (dict) -- mapping of DirResponse to their rounded count
    dir_v3_direct_dl (dict) -- mapping of DirStat to measurement over DirPort
    dir_v3_tunneled_dl (dict) -- mapping of DirStat to measurement over ORPort

Omitted: 
    Anything with v2
    dir_v3_responses_unknown (dict) -- mapping of unrecognized statuses to their count
    dir_v3_tunneled_dl_unknown (dict) -- mapping of unrecognized stats to their measurement
    dir_v3_direct_dl_unknown (dict) -- mapping of unrecognized stats to their measurement
"""


def extract_directory_stats(desc, entry):
    entry["end"] = desc.dir_stats_end
    entry["start"] = desc.dir_stats_end - datetime.timedelta(
        seconds=desc.dir_stats_interval
    )
    entry["dir_v3_share"] = desc.dir_v3_share
    resp = dict(entry)
    if (
        desc.dir_v3_responses is None
        or desc.dir_v3_direct_dl is None
        or desc.dir_v3_tunneled_dl is None
    ):
        print("Error parsing dir_v3 dicts")
        return dict(), dict(), dict()
    for k, v in desc.dir_v3_responses.items():
        resp[k.replace("-", "_")] = v
    direct = dict(entry)
    for k, v in desc.dir_v3_direct_dl.items():
        direct[k.replace("-", "_")] = v
    tunnelled = dict(entry)
    for k, v in desc.dir_v3_tunneled_dl.items():
        tunnelled[k.replace("-", "_")] = v
    return resp, direct, tunnelled


"""
Directory Mirror Attributes:
Variables:	
    dir_stats_end (datetime) -- end of the period when stats were gathered
    dir_stats_interval (int) -- length in seconds of the interval
    dir_v3_share (float) -- percent of total directory traffic it expects to serve
    dir_v3_ips (dict) -- mapping of locales to rounded count of requester ips
    dir_v3_requests (dict) -- mapping of locales to rounded count of requests
"""


def extract_directory_locales(desc, entry):
    entry["end"] = desc.dir_stats_end
    entry["start"] = desc.dir_stats_end - datetime.timedelta(
        seconds=desc.dir_stats_interval
    )
    entry["dir_v3_share"] = desc.dir_v3_share
    if desc.dir_v3_ips is None or desc.dir_v3_requests is None:
        print("Error parsing dir_v3 requests")
        return list(), list()
    ipList = extract_dict(desc, "dir_v3_ips", "locale", "rounded_requester_ips", entry)
    reqList = extract_dict(
        desc, "dir_v3_requests", "locale", "rounded_request_count", entry
    )
    return ipList, reqList


def process_extrainfo_entries(entries, bDB, should_index=False, whitelist=None):
    bDB.setup_queues(
        [
            "conn_bi_direct",
            "relay_write_history",
            "relay_read_history",
            "relay_cell_history",
            "dir_read_history",
            "dir_write_history",
            "entry_stats",
            "exit_written",
            "exit_read",
            "exit_streams",
            "hs_stats",
            "padding_counts",
            "bridge_ips",
            "bridge_protos",
            "dir_v3_responses",
            "dir_v3_direct",
            "dir_v3_tunneled",
            "dir_v3_ips",
            "dir_v3_requests",
        ]
    )
    l_has_attribute = lambda x, y: has_attribute(x, y, whitelist=whitelist)
    for desc in entries:
        core = extract_core(desc)
        if l_has_attribute(desc, "conn_bi_direct_end"):
            bDB.insert("conn_bi_direct", extract_conn_bi_direct(desc, dict(core)))
        if l_has_attribute(desc, "read_history_end"):
            readList, writeList = extract_relayed_traffic(desc, dict(core))
            bDB.insert_many("relay_write_history", writeList)
            bDB.insert_many("relay_read_history", readList)
        if l_has_attribute(desc, "cell_stats_end"):
            bDB.insert_many(
                "relay_cell_history", extract_relayed_cells(desc, dict(core))
            )
        if l_has_attribute(desc, "dir_read_history_end"):
            rL, wL = extract_dir_bytes(desc, dict(core))
            bDB.insert_many("dir_read_history", rL)
            bDB.insert_many("dir_write_history", wL)
        if l_has_attribute(desc, "entry_stats_end"):
            bDB.insert_many("entry_stats", extract_guard_stats(desc, dict(core)))
        if l_has_attribute(desc, "exit_stats_end"):
            wL, rL, sL = extract_exit_stats(desc, dict(core))
            bDB.insert_many("exit_written", wL)
            bDB.insert_many("exit_read", rL)
            bDB.insert_many("exit_streams", sL)
        if l_has_attribute(desc, "hs_stats_end"):
            bDB.insert("hs_stats", extract_hs_v2_stats(desc, dict(core)))
        if "hidserv-v3-stats-end" in desc._entries.keys():
            bDB.insert("hs_stats", extract_hs_v3_stats(desc, dict(core)))
        if l_has_attribute(desc, "padding_counts_end"):
            bDB.insert_many("padding_counts", extract_padding_counts(desc, dict(core)))
        if l_has_attribute(desc, "bridge_stats_end"):
            iL, pL = extract_bridge_data(desc, dict(core))
            bDB.insert_many("bridge_ips", iL)
            bDB.insert_many("bridge_protos", pL)
        if l_has_attribute(desc, "dir_stats_end"):
            r, d, t = extract_directory_stats(desc, dict(core))
            bDB.insert("dir_v3_responses", r)
            bDB.insert("dir_v3_direct", d)
            bDB.insert("dir_v3_tunneled", t)
            ipL, reqL = extract_directory_locales(desc, dict(core))
            bDB.insert_many("dir_v3_ips", ipL)
            bDB.insert_many("dir_v3_requests", reqL)
        bDB.process_queues(100000)
    bDB.flush_queues()
    for tname in bDB.get_tables():
        if bDB.has_column(tname, "start") and bDB.has_column(tname, "end"):
            # The removal of rows can be done in a single linear scan then update. Consequently index second.
            bDB.removeDuplicateRows(tname, ["fingerprint", "start", "end"])
            if should_index:
                bDB.createIndex(tname, ["fingerprint", "start", "end"])
        else:
            if should_index:
                bDB.createIndex(tname, ["fingerprint"])
