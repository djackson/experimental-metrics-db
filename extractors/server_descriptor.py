"""
    From: https://stem.torproject.org/api/descriptor/server_descriptor.html

    nickname (str) -- * relay's nickname
    fingerprint (str) -- identity key fingerprint
    published (datetime) -- * time in UTC when this descriptor was made
    address (str) -- * IPv4 address of the relay
    or_port (int) -- * port used for relaying
    socks_port (int) -- * port used as client (deprecated, always None)
    dir_port (int) -- * port used for descriptor mirroring
    platform (bytes) -- line with operating system and tor version
    tor_version (stem.version.Version) -- version of tor
    operating_system (str) -- operating system
    uptime (int) -- uptime when published in seconds
    contact (bytes) -- contact information
    exit_policy (stem.exit_policy.ExitPolicy) -- * stated exit policy
    exit_policy_v6 (stem.exit_policy.MicroExitPolicy) -- * exit policy for IPv6
    bridge_distribution (BridgeDistribution) -- * preferred method of providing this relay's address if a bridge
    family (set) -- * nicknames or fingerprints of declared family
    average_bandwidth (int) -- * average rate it's willing to relay in bytes/s
    burst_bandwidth (int) -- * burst rate it's willing to relay in bytes/s
    observed_bandwidth (int) -- * estimated capacity based on usage in bytes/s
    link_protocols (list) -- link protocols supported by the relay
    circuit_protocols (list) -- circuit protocols supported by the relay
    is_hidden_service_dir (bool) -- * indicates if the relay serves hidden service descriptors
    hibernating (bool) -- * hibernating when published
    allow_single_hop_exits (bool) -- * flag if single hop exiting is allowed
    allow_tunneled_dir_requests (bool) -- * flag if tunneled directory requests are accepted
    extra_info_cache (bool) -- * flag if a mirror for extra-info documents
    extra_info_digest (str) -- upper-case hex encoded digest of our extra-info document
    extra_info_sha256_digest (str) -- base64 encoded sha256 digest of our extra-info document
    eventdns (bool) -- flag for evdns backend (deprecated, always unset)
    ntor_onion_key (str) -- base64 key used to encrypt EXTEND in the ntor protocol
    or_addresses (list) -- * alternative for our address/or_port attributes, each entry is a tuple of the form (address (str), port (int), is_ipv6 (bool))
    protocols (dict) -- mapping of protocols to their supported versions
"""

# all_attributes = ['nickname', 'fingerprint', 'published', 'address', 'or_port', 'socks_port', 'dir_port', 'platform', 'tor_version', 'operating_system', 'uptime', 'contact', 'exit_policy', 'exit_policy_v6', 'bridge_distribution', 'family', 'average_bandwidth', 'burst_bandwidth', 'observed_bandwidth', 'link_protocols', 'circuit_protocols', 'is_hidden_service_dir', 'hibernating', 'allow_single_hop_exits', 'allow_tunneled_dir_requests', 'extra_info_cache',  'eventdns', 'ntor_onion_key', 'or_addresses', 'protocols']

# Not handled: link_protocols, circuit_protocols, extra_info_digest, extra_info_sha256_digest, eventdns, protocols

# Specially handled: Exit Policy, Exit Policy v6, or_addresses, family,

import hashlib


def extract_server_descriptor(desc):
    selected_attributes = [
        "nickname",
        "fingerprint",
        "published",
        "address",
        "or_port",
        "socks_port",
        "dir_port",
        "platform",
        "operating_system",
        "uptime",
        "contact",
        "average_bandwidth",
        "burst_bandwidth",
        "observed_bandwidth",
        "is_hidden_service_dir",
        "hibernating",
        "allow_single_hop_exits",
        "allow_tunneled_dir_requests",
        "extra_info_cache",
        "ntor_onion_key",
    ]
    out = dict()
    for k in selected_attributes:
        v = getattr(desc, k)
        out[k] = v
    out["tor_version"] = desc.tor_version.version_str
    if len(desc.or_addresses) > 0:
        r = desc.or_addresses.pop()
        out["additional_address_ip"] = r[0]
        out["additional_address_port"] = r[1]
        out["additional_address_is_ipv6"] = r[2]
    if len(desc.or_addresses) > 1:
        out["additional_address_overflow"] = True
    out["exit_policy"] = str(desc.exit_policy)
    out["exit_policy_v6"] = str(desc.exit_policy_v6)
    out["exit_allowed"] = desc.exit_policy.is_exiting_allowed()
    out["exit_web_allowed"] = desc.exit_policy.can_exit_to(
        port=80
    ) and desc.exit_policy.can_exit_to(port=443)
    out["exit_v6_allowed"] = desc.exit_policy_v6.is_exiting_allowed()
    out["exit_v6_web_allowed"] = desc.exit_policy_v6.can_exit_to(
        port=80
    ) and desc.exit_policy_v6.can_exit_to(port=443)
    out["family_fingerprint"] = family_fingerprint(desc)
    out["family_count"] = len(desc.family) + 1
    return out


def extract_families(desc):
    family_entries = list()
    for f in desc.family:
        f_row = dict()
        f_row["fingerprint"] = desc.fingerprint
        f_row["published"] = desc.published
        f_row["family_member"] = f[1:]
        family_entries.append(f_row)
    return family_entries


def family_fingerprint(desc):
    l = list(desc.family)
    l = [x[1:] for x in l]
    l.append(desc.fingerprint)
    l.sort()
    familyString = ",".join(l)
    hO = hashlib.sha256(familyString.encode())
    return hO.hexdigest()


def process_descriptor_entries(entries, bDB, should_index=False, whitelist=None):
    servdescT = "server_descriptors"
    familiesT = "families"
    bDB.setup_queues([servdescT, familiesT])
    for desc in entries:
        bDB.insert(servdescT, extract_server_descriptor(desc))
        bDB.insert_many(familiesT, extract_families(desc))
        bDB.process_queues(50000)
    bDB.flush_queues()
    bDB.removeDuplicateRows(servdescT, ["fingerprint", "published"])
    bDB.removeDuplicateRows(familiesT, ["fingerprint", "published", "family_member"])
    if should_index:
        bDB.createIndex(servdescT, ["fingerprint", "published"])
        bDB.createIndex(servdescT, ["family_fingerprint", "published"])
