"""
 class stem.descriptor.bandwidth_file.BandwidthFile(raw_content, validate=False)[source]

    Bases: stem.descriptor.Descriptor

    Tor bandwidth authority measurements.
    Variables:	

        measurements (dict) -- * mapping of relay fingerprints to their bandwidth measurement metadata

        header (dict) -- * header metadata
            Mostly repeats of below. 

        timestamp (datetime) -- * time when these metrics were published
        version (str) -- * document format version
        software (str) -- application that generated these metrics
        software_version (str) -- version of the application that generated these metrics
        earliest_bandwidth (datetime) -- time of the first sampling
        latest_bandwidth (datetime) -- time of the last sampling
        created_at (datetime) -- time when this file was created
        generated_at (datetime) -- time when collection of these metrics started
        consensus_size (int) -- number of relays in the consensus
        eligible_count (int) -- relays with enough measurements to be included
        eligible_percent (int) -- percentage of consensus with enough measurements
        min_count (int) -- minimum eligible relays for results to be provided
        min_percent (int) -- minimum measured percentage of the consensus
        scanner_country (str) -- country code where this scan took place
        destinations_countries (list) -- all country codes that were scanned
        time_to_report_half_network (int) -- estimated number of seconds required to measure half the network, given recent measurements
        recent_stats (RecentStats) -- statistical information collected over the last 'data_period' (by default five days)

            
    class stem.descriptor.bandwidth_file.RecentStats[source]

        Bases: object

        Statistical information collected over the last 'data_period' (by default five days).
        Variables:	

            consensus_count (int) -- number of consensuses published during this period
            prioritized_relays (int) -- number of relays prioritized to be measured
            prioritized_relay_lists (int) -- number of times a set of relays were prioritized to be measured
            measurement_attempts (int) -- number of relay measurements we attempted
            measurement_failures (int) -- number of measurement attempts that failed
            relay_failures (RelayFailures) -- number of relays we failed to measure

    class stem.descriptor.bandwidth_file.RelayFailures[source]

        Bases: object

        Summary of the number of relays we were unable to measure.
        Variables:	

            no_measurement (int) -- number of relays that did not have any successful measurements
            insuffient_period (int) -- number of relays whos measurements were collected over a period that was too small (1 day by default)
            insufficient_measurements (int) -- number of relays we did not collect enough measurements for (2 by default)
            stale (int) -- number of relays whos latest measurement is too old (5 days by default)
"""

# TODO Who made this consensus?


def extract_measurement_metadata(desc):
    entry = dict()
    copied_entries = [
        "timestamp",
        "version",
        "software",
        "software_version",
        "earliest_bandwidth",
        "latest_bandwidth",
        "created_at",
        "generated_at",
        "consensus_size",
        "eligible_count",
        "eligible_percent",
        "min_count",
        "min_percent",
        "scanner_country",
        "time_to_report_half_network",
    ]
    for k in copied_entries:
        entry[k] = getattr(desc, k)
    if desc.destinations_countries is not None:
        entry["destinations_countries"] = ",".join(
            getattr(
                desc,
                "destinations_countries",
            )
        )

    recent_entries = [
        "consensus_count",
        "prioritized_relays",
        "measurement_attempts",
        "measurement_failures",
    ]
    for k in recent_entries:
        entry[k] = getattr(desc.recent_stats, k)
    failure_entries = [
        "no_measurement",
        "insuffient_period",
        "insufficient_measurements",
        "stale",
    ]
    for k in failure_entries:
        entry[k] = getattr(desc.recent_stats.relay_failures, k)
    return entry


"""
Measurements data

        Keys are:
        {'bw': '1', 'error_circ': '1', 'error_destination': '0', 'error_misc': '0', 'error_second_relay': '0', 'error_stream': '0', 'master_key_ed25519': 'ejSqbIvzi2KLu3deILncMZcUTyi9h/OTKqscwyWoM2I', 'nick': 'Unnamed', 'node_id': '$28F1F5E8607B5EC49C111543EBCCF06ED5947F24', 'relay_in_recent_consensus_count': '1', 'relay_recent_measurement_attempt_count': '1', 'relay_recent_measurements_excluded_error_count': '1', 'relay_recent_priority_list_count': '1', 'success': '0', 'time': '2021-02-05T12:45:46', 'unmeasured': '1', 'vote': '0'}
"""

# TODO tie to a particular bandwidth run and measurer
def extract_measurements(desc, base):
    this_base = dict(base)
    for k, m in desc.measurements.items():
        new_m = dict(this_base)
        new_m.update(m)
        # del new_m["node_id"]
        # del new_m["master_key_ed25519"]
        new_m["fingerprint"] = k
        yield new_m


# TODO Stem has a memory usage bug when parsing bandwidth files?
def process_bandwidth_entries(entries, bDB, should_index=False, whitelist=None):
    measurement_runsT = "bandwith_measurement_runs"
    measurementsT = "bandwidth_measurements"
    bDB.setup_queues([measurement_runsT, measurementsT])
    for desc in entries:
        core = extract_measurement_metadata(desc)
        bDB.insert(measurement_runsT, core)
        bDB.insert_many(measurementsT, extract_measurements(desc, core))
        bDB.process_queues(50000)
    bDB.flush_queues()
    bDB.removeDuplicateRows(measurement_runsT, ["timestamp"])  # TODO Handle ID
    bDB.removeDuplicateRows(
        measurementsT, ["timestamp", "fingerprint"]
    )  # TODO Measurerer ID
    if should_index:
        bDB.createIndex(measurement_runsT, ["timestamp"])
        bDB.createIndex(measurementsT, ["timestamp", "fingerprint"])