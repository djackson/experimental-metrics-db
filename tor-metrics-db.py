#!/usr/bin/env python3

import argparse
from datetime import datetime
import os
from tqdm import tqdm
from extractors import *
from fetch import *
from bufferedDB import *

known_types = {
    "network-status-consensus-3": process_consensus_entries,
    "server-descriptor": process_descriptor_entries,
    "extra-info": process_extrainfo_entries,
    "bandwidth-file": process_bandwidth_entries,
}


def get_arguments():
    description = "A script to fetch records collected by CollecTor, parse them using Stem and store them in a database"
    epilog = ""
    valid_choices = list(known_types.keys())
    wildcard = "ALL"
    valid_choices.append(wildcard)
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument(
        "-db",
        "--database",
        default="sqlite:///out/test.db",
        help="The sqlite or postgres database url you would like to output to",
    )
    parser.add_argument(
        "--start",
        required=True,
        help="The date DD-MM-YYYY you would like to begin from",
    )
    parser.add_argument(
        "--end", default=None, help="The date DD-MM-YYYY you would like to end from"
    )
    parser.add_argument(
        "--only",
        action="append",
        choices=valid_choices,
        help="The types of documents you would like to parse.",
        default=[],
        dest="selected",
    )
    parser.add_argument(
        "--whitelist",
        action="append",
        help="If set, the types of extra-info descriptors you would like to store",
        default=None,
        dest="whitelist",
    )
    parser.add_argument(
        "--cache",
        default="data/",
        help="A directory in which to store temporary files downloaded from CollectTor, speeding up subsequent runs.",
    )
    parser.add_argument(
        "--cache-only",
        default=False,
        action="store_true",
        help="Don't attempt to download files from the network.",
        dest="cache_only",
    )
    parser.add_argument(
        "--fresh",
        default=False,
        action="store_true",
        help="Drop the database and the list of parsed CollecTor files.",
    )
    parser.add_argument(
        "--index",
        default=False,
        action="store_true",
        help="Build indexes for each table.",
    )
    # TODO Add the ability to wipe the database (and caches)
    args = parser.parse_args()
    if len(args.selected) == 0:
        args.selected = [wildcard]
    if wildcard in args.selected:
        args.selected = known_types.keys()
    args.start = datetime.strptime(args.start, "%d-%m-%Y")
    if args.end:
        args.end = datetime.strptime(args.end, "%d-%m-%Y")
    return args


def getConcurrentGenerator(gen, description):
    return tqdm(gen, leave=True, desc=description, smoothing=0.0)


def main():
    # TODO Create needed directories
    args = get_arguments()
    bDB = bufferedDB(args.database, args.fresh)
    for k in args.selected:
        with getConcurrentGenerator(
            get_entries_concurrent(
                k, args.start, args.end, args.cache, args.cache_only, args.fresh
            ),
            f"Processing {k}",
        ) as g:
            known_types[k](g, bDB, should_index=args.index, whitelist=args.whitelist)

    stats = bDB.get_stats()
    sL = list(stats.items())
    sL.sort(key=lambda x: -x[1])
    for k, v in sL:
        print(f"{v} {k} entries processed")


if __name__ == "__main__":
    main()
