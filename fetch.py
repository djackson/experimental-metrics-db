import stem.descriptor.collector
import stem.descriptor.reader
import os
from datetime import datetime
from tqdm import tqdm

"""
This function allows for the entries to be fetched and parsed by stem concurrently.
It also supports persistent state. #TODO Support for wiping all the caches and output.
"""


def get_entries_concurrent(name, start_date, end_date, cache_dir, cache_only, fresh):
    if name in ["network-status-consensus-3"]:
        document_handler = "DOCUMENT"
    else:
        document_handler = None

    collector = stem.descriptor.collector.CollecTor()
    out_dir = os.path.join(cache_dir, name)
    persis_path = os.path.join(out_dir, "processedFiles.txt")
    if fresh and os.path.exists(persis_path):
        os.remove(persis_path)
    assert start_date is not None
    if end_date is None:
        end_date = datetime.now()

    if not cache_only:
        for f in tqdm(
            collector.files(name, start=start_date, end=end_date),
            desc="Downloading descriptors",
            leave=True,
        ):
            f.download(out_dir, overwrite=True)

    with stem.descriptor.reader.DescriptorReader(
        out_dir,
        validate=False,
        buffer_size=10,
        persistence_path=persis_path,
        document_handler=document_handler,
    ) as reader:
        for desc in reader:
            # TODO Feels like stem should be able to handle this for us, not sure what's happened here.
            if hasattr(desc, "published") and desc.published is not None:
                if desc.published < start_date or desc.published > end_date:
                    continue
            if hasattr(desc, "valid_until") and desc.valid_until is not None:
                if desc.valid_until < start_date:
                    continue
            if hasattr(desc, "valid_after") and desc.valid_after is not None:
                if desc.valid_after > end_date:
                    continue
            yield desc


"""
This function does the stem parsing in the same thread. 
It appears to be the same speed as stem normally, but slower than 
the concurrent process when it has to skip a lot of data due to start/end dates.
It does not use a thread or caching of read files. 
"""


def get_entries(name, start_date, end_date, cache_dir, cache_only, fresh):
    if name in ["network-status-consensus-3"]:
        document_handler = "DOCUMENT"
    else:
        document_handler = None
    collector = stem.descriptor.collector.CollecTor()
    cache_dir = os.path.join(cache_dir, name)
    # Fresh is ignored since this function does not persist read files.
    if not cache_only:
        for f in tqdm(
            collector.files(name, start=start_date, end=end_date),
            desc="Downloading descriptors",
        ):
            f.download(cache_dir, overwrite=True)
    assert start_date is not None
    if end_date is None:
        end_date = datetime.now()
    for f in collector.files(name, start=start_date, end=end_date):
        for desc in f.read(
            cache_dir, name, start_date, end_date, document_handler=document_handler
        ):
            if hasattr(desc, "published") and desc.published is not None:
                if desc.published < start_date or desc.published > end_date:
                    continue
            if hasattr(desc, "valid_until") and desc.valid_until is not None:
                if desc.valid_until < start_date:
                    continue
            if hasattr(desc, "valid_after") and desc.valid_after is not None:
                if desc.valid_after > end_date:
                    continue
            yield desc
