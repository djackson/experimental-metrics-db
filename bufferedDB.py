import dataset
from collections import Counter
from queue import Queue
from threading import Thread
import os
import time


class concurrentbufferedDB:
    def __init__(self, db_url, fresh, table_prefix=""):
        if fresh:
            assert "sqlite://" in db_url
            os.remove(db_url.split(":///")[-1])
        self.db = dataset.connect(db_url)
        self.db_url = db_url
        self.insert_queues = dict()
        self.table_prefix = table_prefix
        self.stats = Counter()
        self.halt_writing_thread = False

    # TODO Disable/Remove Indexes when setting up queues?

    def process_queues(self, limit):
        pass

    def _internal_process_queues(self, limit):
        changes = False
        for k, v in self.insert_queues.items():
            if v.qsize() < limit:
                continue
            buff = list()
            for i in range(limit):
                buff.append(v.get())
            self.stats[k] += len(buff)
            self.db[self.table_prefix + k].insert_many(
                buff, chunk_size=limit, ensure=True
            )
            for b in buff:
                v.task_done()
            changes = True
        if not changes:
            time.sleep(1)

    def _get_queue(self, key):
        return self.insert_queues[key]

    def setup_queues(self, queues):
        self.db.begin()
        for q in queues:
            self.insert_queues[q] = Queue(maxsize=150000)
            self.db.create_table(self.table_prefix + q)
        self._start_writing_thread()

    def insert(self, table, item):
        self._get_queue(table).put(item)

    def insert_many(self, table, items):
        for i in items:
            self.insert(table, i)

    def flush_queues(self):
        for v in self.insert_queues.values():
            v.join()
        self.halt_writing_thread = True
        self.db.commit()

    def get_stats(self):
        return self.stats

    def get_tables(self):
        return self.insert_queues.keys()

    def has_column(self, table, col):
        return self.db[table].has_column(col)

    def get_db(self):
        return self.db

    def _concurrent_process_queue(self):
        while not self.halt_writing_thread:
            self._internal_process_queues(50000)

    def _start_writing_thread(self):
        self.halt_writing_thread = False
        self.writing_thread = Thread(target=self._concurrent_process_queue)
        self.writing_thread.daemon = True
        self.writing_thread.start()

    def createIndex(self, table, cols):
        assert "sqlite://" in self.db_url  # TODO Necessary?
        self.db[table].create_index(cols)

    def removeDuplicateRows(self, table, cols):
        assert "sqlite://" in self.db_url or "postgresql://" in self.db_url
        if "sqlite://" in self.db_url:
            sql = """ 
            delete   from {table}
                where    rowid not in
                (
                select  min(rowid)
                from    {table}
                GROUP BY {columns}
                );
            """
            if len(cols) == 1:
                cols = cols[0]
            else:
                cols = ",".join(cols)
        elif "postgresql://" in self.db_url:
            sql = """DELETE FROM {table} T1
            USING   {table} T2
            WHERE   T1.ctid < T2.ctid 
            AND {columns}; 
            """
            if len(cols) == 1:
                cols = f"T1.{cols[0]}=T2.{cols[0]}"
            else:
                cols = " AND ".join([f"T1.{i} = T2.{i}" for i in cols])
        sql = sql.format(table=table, columns=cols)
        self.db.query(sql)


class bufferedDB:
    def __init__(self, db_url, fresh, table_prefix=""):
        if fresh:
            assert "sqlite://" in db_url
            sqlite_file = db_url.split(":///")[-1]
            if os.path.exists(sqlite_file):
                os.remove(sqlite_file)
        self.db = dataset.connect(db_url)
        self.db_url = db_url
        self.insert_queues = dict()
        self.table_prefix = table_prefix
        self.stats = Counter()

    # TODO Disable/Remove Indexes when setting up queues?

    def process_queues(self, limit):
        self._internal_process_queues(limit)

    def _internal_process_queues(self, limit):
        for k in self.insert_queues.keys():
            v = self.insert_queues[k]
            if len(v) < limit:
                continue
            self.stats[k] += len(v)
            self.db[self.table_prefix + k].insert_many(v, chunk_size=limit, ensure=True)
            self.insert_queues[k] = list()

    def _get_queue(self, key):
        return self.insert_queues[key]

    def setup_queues(self, queues):
        self.db.begin()
        for q in queues:
            self.insert_queues[q] = list()
            self.db.create_table(self.table_prefix + q)

    def insert(self, table, item):
        self._get_queue(table).append(item)

    def insert_many(self, table, items):
        self._get_queue(table).extend(items)

    def flush_queues(self):
        self.process_queues(0)
        self.db.commit()

    def get_stats(self):
        return self.stats

    def get_tables(self):
        return self.insert_queues.keys()

    def has_column(self, table, col):
        return self.db[table].has_column(col)

    def get_db(self):
        return self.db

    def createIndex(self, table, cols):
        assert "sqlite://" in self.db_url  # TODO Necessary?
        self.db[table].create_index(cols)

    def removeDuplicateRows(self, table, cols):
        assert "sqlite://" in self.db_url or "postgresql://" in self.db_url
        if "sqlite://" in self.db_url:
            sql = """ 
            delete   from {table}
                where    rowid not in
                (
                select  min(rowid)
                from    {table}
                GROUP BY {columns}
                );
            """
            if len(cols) == 1:
                cols = cols[0]
            else:
                cols = ",".join(cols)
        elif "postgresql://" in self.db_url:
            sql = """DELETE FROM {table} T1
            USING   {table} T2
            WHERE   T1.ctid < T2.ctid 
            AND {columns}; 
            """
            if len(cols) == 1:
                cols = f"T1.{cols[0]}=T2.{cols[0]}"
            else:
                cols = " AND ".join([f"T1.{i} = T2.{i}" for i in cols])
        sql = sql.format(table=table, columns=cols)
        self.db.query(sql)
